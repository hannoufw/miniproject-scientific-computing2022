import json
import time
from rich import print
from rich.table import Table





THE_NUMBER_OF_Studio = 2
THE_NUMBER_OF_Chambre = 2
ROOM_RESERVATION = []
HOTEL_EMAIL = 'wassimhotel@gmail.com'
HOTEL_PASSWORD = 'wassimhotelpassword'

def homepage_prompt():
    print("\n\n\n         [red]welcome to Wassim Hotel[/red] :house:                    \n [blue]choose the appropriate number for your request[/blue]              \n")
    choice = ""
    while choice not in ['1', '2','3','4']:
        

        table = Table(show_header=True, header_style="bold magenta", style="black", show_lines=True)
        table.add_column("number", style="blue", justify="center", width=10)
        table.add_column("Choice", style="blue", width=30)
        table.add_row("(1)", "For Room Reservation")
        table.add_row("(2)", "For Administration")
        table.add_row("(3)", "For Contact")
        table.add_row("(4)", "For Exit")
        print(table)
        

        choice = input("")
        
        if choice == '4':
            exit()
        
        if choice == '1':
            if(THE_NUMBER_OF_Studio>0 and THE_NUMBER_OF_Chambre==0):
                print("   [red]WE ONLY HAVE STUDIO AVAILABE[/red]  \n")
                time.sleep(1)
                reservation_prompt()
            if(THE_NUMBER_OF_Chambre>0 and THE_NUMBER_OF_Studio==0):
                print("   [red]WE ONLY HAVE CHAMBRE AVAILABE[/red]  \n")
                time.sleep(1)
                reservation_prompt()
            if(THE_NUMBER_OF_Chambre==0 and THE_NUMBER_OF_Studio==0):
                print("   [red]THERE IS NO ROOMS AVAILABLE[/red] \n   [red]THANK YOU[/red] \n ")
                time.sleep(3)
                homepage_prompt()
            
            reservation_prompt()
        
        if choice == '2':
            hotel_email = input("enter you email :")
            while hotel_email != HOTEL_EMAIL:
                hotel_email = input("Invalid email , please enter you email (q for exit) :")
                if(hotel_email=='q'):
                    homepage_prompt()
            hotel_password = input("enter your password :")
            while hotel_password != HOTEL_PASSWORD:
                hotel_password = input("Invalid password, please enter your password (q for exit):")
                if(hotel_password=='q'):
                    homepage_prompt()
            print("\n           [red]Hello Administrator[/red]        \n")
            administrator_prompt()
        if choice == '3':
            table0 = Table(show_header=True, header_style="bold magenta", style="black", show_lines=True)
            table0.add_column("Contact", style="blue", width=15)
            table0.add_column("Use", style="blue", width=25)
            table0.add_row("By EMAIL", "wassimhotel@gmail.com")
            table0.add_row("By PHONE", "+33 (0) 6 19 17 47 81")
            print(table0)

            time.sleep(3)
            homepage_prompt()


        

def reservation_prompt():
    global THE_NUMBER_OF_Studio
    global THE_NUMBER_OF_Chambre
    roomtype = ""
    if(THE_NUMBER_OF_Studio>0 and THE_NUMBER_OF_Chambre==0):

        table1 = Table(title="Reserve a Room", show_header=True, header_style="bold magenta", style="black", show_lines=True)
        table1.add_column("number", style="blue", justify="center", width=10)
        table1.add_column("room type", style="blue", width=15)
        table1.add_column("Price", style="blue", width=15)
        table1.add_row("(1)", "studio", "45$")
        table1.add_row("(3)", "EXIT")
        print(table1)
        
        roomtype = input("")
        if roomtype=='1':
           THE_NUMBER_OF_Studio =  THE_NUMBER_OF_Studio-1
        elif roomtype=='3':
                homepage_prompt()
        else:
                reservation_prompt()
    elif THE_NUMBER_OF_Chambre>0 and THE_NUMBER_OF_Studio==0:

            table2 = Table(title="Reserve a Room", show_header=True, header_style="bold magenta" , style="black", show_lines=True)
            table2.add_column("number", style="blue", justify="center", width=10)
            table2.add_column("room type", style="blue", width=15)
            table2.add_column("Price", style="blue", width=15)
            table2.add_row("(2)", "chambre", "35$")
            table2.add_row("(3)", "Exit",)
            print(table2)

            roomtype = input("")
        
            if roomtype=='2':
                THE_NUMBER_OF_Chambre =  THE_NUMBER_OF_Chambre-1
            elif roomtype=='3':
                homepage_prompt()
            else:
                reservation_prompt()
    else:
            THE_NUMBER_OF_Chambre>0 and THE_NUMBER_OF_Studio>0

            table3 = Table(title="Reserve a Room", show_header=True, header_style="bold magenta" , style="black", show_lines=True)
            table3.add_column("number", style="blue", justify="center", width=10)
            table3.add_column("room type", style="blue", width=15)
            table3.add_column("Price", style="blue", width=15)
            table3.add_row("(1)", "studio", "45$")
            table3.add_row("(2)", "chambre", "35$")
            table3.add_row("(3)", "Exit",)
            print(table3)
            roomtype = input("")

            if roomtype=='1':
                THE_NUMBER_OF_Studio =  THE_NUMBER_OF_Studio-1
            elif roomtype=='2':
                THE_NUMBER_OF_Chambre =  THE_NUMBER_OF_Chambre-1 
            elif roomtype=='3':
                homepage_prompt()
            else:
                    reservation_prompt()
    reservation1 = input("Name :")
    phone_number = input("phone number :")
    email = input("email :")
    date = input("Date(day/month/year) :")

    ROOM_RESERVATION.append(
        {
            'roomtype': roomtype,
            'Name': reservation1,
            'phone number': phone_number,
            'email': email,
            'Date(day/month/year)': date,
        }
    )
    with open('data.json', 'w') as file:
        json.dump(ROOM_RESERVATION, file)

    print("\n [red]Your Reservation is done \n Pay cash upon arrival at the Hotel \n THANK YOU[/red]")
    time.sleep(3)
    homepage_prompt()




def administrator_prompt():
    administrator_choice = ''
    while administrator_choice not in ['1', '2']:

        table4 = Table(show_header=True, header_style="bold magenta", style="black", show_lines=True)
        table4.add_column("number", style="blue", justify="center", width=10)
        table4.add_column("Choice", style="blue", width=30)
        table4.add_row("(1)", "See Reservations List")
        table4.add_row("(2)", "Delete a Reservation")
        table4.add_row("(3)", "Home Page")
        print(table4)

        administrator_choice = input("")
        if administrator_choice == '3':
            homepage_prompt()
        if administrator_choice == '1':
            print(f"             Reservations list           \n")
            for room in ROOM_RESERVATION:
                if room['roomtype'] == '1':
                 print(f"  Name : {room['Name']}\n"
                        f"  roomtype : {'Studio'}\n"
                        f"  phone number : {room['phone number']}\n"
                        f"  email : {room['email']}\n"
                        f"  Date(day/month/year) : {room['Date(day/month/year)']}\n"
                        f"\n")
                else :
                    if room['roomtype'] == '2':
                      print(f"  Name : {room['Name']}\n"
                        f"  roomtype : {'Chambre'}\n"
                        f"  phone number : {room['phone number']}\n"
                        f"  email : {room['email']}\n"
                        f"  Date(day/month/year) : {room['Date(day/month/year)']}\n"
                        f"\n")
            time.sleep(3)
        if administrator_choice == '2':
            name1 = input("enter the name of reservation you want to delete it:")
            findname = False
            for i in range(0, len(ROOM_RESERVATION)):
            
                if ROOM_RESERVATION[i]['Name'] == name1:
                    del ROOM_RESERVATION[i]
                    findname = True
                    with open('data.json', 'w') as file:
                        json.dump(ROOM_RESERVATION, file)
                    print("\n[red]The reservation has been deleted[/red]")
                    time.sleep(3)
                    administrator_prompt()
            if findname == False:
                print("[red]There is no reservation in this name[/red]")
                time.sleep(3)
                administrator_prompt()

        
            
        administrator_prompt()


if __name__ == '__main__':
    
        homepage_prompt()
